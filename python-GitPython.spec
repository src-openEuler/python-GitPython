%global _empty_manifest_terminate_build 0
Name:		python-GitPython
Version:	3.1.43
Release:	2
Summary:	GitPython is a python library used to interact with Git repositories
License:	BSD-3-Clause
URL:		https://github.com/gitpython-developers/GitPython
Source0:	https://github.com/gitpython-developers/GitPython/archive/refs/tags/3.1.43.tar.gz
BuildArch:	noarch

%description
GitPython is a python library used to interact with git repositories,
high-level like git-porcelain, or low-level like git-plumbing.

%package -n python3-GitPython
Summary:	GitPython is a python library used to interact with Git repositories
Provides:	python-GitPython = %{version}-%{release}
# Base build requires
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
# General requires
BuildRequires:	python3-gitdb
# General requires
Requires:	git-core
Requires:	python3-gitdb
Requires:	python3-typing-extensions
%description -n python3-GitPython
GitPython is a python library used to interact with git repositories,
high-level like git-porcelain, or low-level like git-plumbing.

%package help
Summary:	Development documents and examples for GitPython
Provides:	python3-GitPython-doc
%description help
GitPython is a python library used to interact with git repositories,
high-level like git-porcelain, or low-level like git-plumbing.

%prep
%autosetup -n GitPython-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-GitPython -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Dec 18 2024 yangzhenyu <dev11101@linx-info.com> 3.1.43-2
- fix Require git-core

* Thu Jun 13 2024 liuzhilin <liuzhilin@kylinos.cn> - 3.1.43-1
- Update package to version 3.1.43
- Issue and test deprecation warnings.
- Fix version_info cache invalidation, typing, parsing, and serialization.
- Document manual refresh path treatment.
- Improve static typing and docstrings related to git object types.

* Fri Feb 23 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.42-1
- Update package to version 3.1.42
  Report actual attempted Git command when Git.refresh fails

* Fri Nov 03 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.40-1
- Update package to version 3.1.40

* Mon Sep 11 2023 yaoxin <yao_xin001@hoperun.com> - 3.1.32-2
- Fix CVE-2023-41040

* Tue Jul 18 2023 sunhui <sunhui@kylinos.cn> - 3.1.32-1
- Update package to version 3.1.32

* Mon Mar 06 2023 wangjunqi <wangjunqi@kylinos.cn> - 3.1.31-1
- Update package to version 3.1.31

* Wed Feb 8 2023 wubijie <wubijie@kylinos.cn> - 3.1.30-1
- Update package to version 3.1.30

* Tue Dec 06 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.29-1
- Update package to version 3.1.29

* Tue May 31 2022 OpenStack_SIG <openstack@openeuler.org> - 3.1.27-1
- Upgrade package python3-GitPython to version 3.1.27

* Tue May 10 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 3.1.14-2
- License compliance rectification

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org> - 3.1.14-1
- update to 3.1.14

* Mon Oct 26 2020 Jiachen Fan <fanjiachen3@huawei.com> - 3.1.11-1
- package init
